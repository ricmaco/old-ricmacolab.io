[![pipeline status](https://gitlab.com/ricmaco/ricmaco.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/ricmaco/ricmaco.gitlab.io/commits/master)

# RicMa.co

Hello, this is the source code for my site.

It is built around [Pelican](https://blog.getpelican.com/), a static site 
generator written in Python.

## Build

In order to build the site you have to install `virtualenv` and 
`virtualenvwrapper` (or similar, such as `virtualfish`).

Then a `pelican` virtualenv has to be created

```bash
$ mkvirtualenv site -p /usr/bin/python3
$ pip install pelican
```

To access the newly created `site` virtualenv just type

```bash
$ workon site
```

To finally build the site you type

```bash
$ workon site
$ pelican
```

## Display

To display site to see a development preview, use this

```bash
$ workon site
$ cd public
$ python -m pelican.server & >/dev/null 2>&1
$ firefox http://localhost:8000/
```
