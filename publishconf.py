#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals
import os
import sys
sys.path.append(os.curdir)

from pelicanconf import *

# analytics
GOOGLE_ANALYTICS = 'UA-78470333-1'

# no debug
RELATIVE_URLS = False
USE_LESS = False
