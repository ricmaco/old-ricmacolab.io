#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals

AUTHOR = 'Riccardo Macoratti'
SITENAME = 'RicMa.co'
SITETITLE = 'Riccardo Macoratti'
SITEURL = 'https://ricma.co'
SITESUBTITLE = 'Geeky developer · Linux lover'
SITEDESCRIPTION = 'Ingenious ideas, shared.'
SITELOGO = 'images/photo.jpg'
FAVICON = '/favicon.ico'

# general
PATH = 'content'
ROBOTS = 'index, follow'
TIMEZONE = 'Europe/Rome'
LOCALE = ('us', 'en_US.utf8')
DEFAULT_LANG = 'en'
DEFAULT_CATEGORY = 'var'
DISPLAY_CATEGORIES_ON_MENU = True
DEFAULT_METADATA = {
  'lang': 'en',
  'status': 'draft'
}
OUTPUT_PATH = 'public'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['articles']
STATIC_PATHS = [
  'raw',
  'files',
  'images',
  'extra',
]
EXTRA_PATH_METADATA = {
  'extra/robots.txt': {
    'path': 'robots.txt'
  },
  'extra/favicon.png': {
    'path': 'favicon.png'
  },
  'extra/favicon.ico': {
    'path': 'favicon.ico'
  },
  'extra/404.html': {
    'path': '404.html'
  }
}
COOKIE_BANNER = True
COOKIE_POLICY = SITEURL + '/pages/cookie-policy.html'
LICENSES = SITEURL + '/pages/licenses.html'
FORMATTED_FIELDS = ['summary']
DELETE_OUTPUT_DIRECTORY = True
DEFAULT_PAGINATION = 10

# sitemap
DIRECT_TEMPLATES = ('index', 'tags', 'categories', 'archives', 'sitemap')
SITEMAP_SAVE_AS = 'sitemap.xml'

# menu
MENUITEMS = (
  ('Categories', '/categories.html'),
  ('Tags', '/tags.html')
)

# links
LINKS = (
  ('cv-eng', 'https://ricmaco.gitlab.io/cv-ng/en/'), 
  ('cv-ita', 'https://ricmaco.gitlab.io/cv-ng/it/'), 
  ('repos', 'https://gitlab.com/ricmaco'),
  ('nixRIOT', 'https://nixriot.wordpress.com'),
  ('linuxvar.it', 'http://linuxvar.it')
)

# social
SOCIAL = (
  ('envelope-o', 'mailto:r.macoratti@gmx.co.uk'),
  ('facebook', 'https://www.facebook.com/ricmaco'),
  ('linkedin', 'https://www.linkedin.com/in/ricmaco'),
  ('twitter', 'https://twitter.com/_ricmaco'),
  ('instagram', 'https://www.instagram.com/_ricmaco'),
  ('youtube', 'https://www.youtube.com/c/RichardMaco00'),
  ('tripadvisor', 'https://www.tripadvisor.com/MemberProfile-a_uid.A6506BCDE553AF4C44C37A6E8D2DC711'),
  ('rss', SITEURL + '/feeds/all.atom.xml')
)

# URLs
AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''

# feed
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# theme
THEME = 'oniony'
MAIN_MENU = True
BROWSER_COLOR = '#333'
COPYRIGHT_YEAR = '2016'
CC_LICENSE = {
  'name': 'Creative Commons Attribution-NonCommercial-ShareAlike',
  'slug': 'by-nc-sa',
  'version': '4.0'
}
PYGMENTS_STYLE = 'pastie'

# debug
RELATIVE_URLS = True
USE_LESS = True
