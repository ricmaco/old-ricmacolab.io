:title: Wicd and Eduroam @ UNIMI
:date: 2014-12-20 11:17
:tags: eduroam, wicd, unimi, statale milano
:status: published
:summary: Making Wicd connection manager and Eduroam network work at
          University of Milan

Wicd_ is an awesome network configuration manager: no clutter, small footprint
and very, very simple (in the UNIX/KISS way).

.. _Wicd: http://wicd.sourceforge.net/

The only problem has come when I had to connect to my university network,
powered by Eduroam_.

.. _Eduroam: http://www.servizi.garr.it/index.php/it/eduroam

My institute has chosen to use WPA2 Enterprise with PEAP/MSCHAPV2 encryption.

To be able to connect with Wicd a configuration file, under path
``/etc/wicd/encryption/templates`` shall be provided.

Go to that path and create a file (as root) named ``eduroam-unimi`` with this content:

.. sourcecode:: sh

  name=Eduroam
  author=Riccardo Macoratti
  version=1.1
  require identity *Name password *Pass
  protected password *Pass
  -----
  ctrl_interface=/var/run/wpa_supplicant
  network={
    ssid="$_ESSID"
    scan_ssid=$_SCAN
    key_mgmt=WPA-EAP
    eap=PEAP
    identity="$_IDENTITY@studenti.unimi.it"
    password="$_PASSWORD"
    phase1="peaplabel=0"
    phase2="auth=MSCHAPV2"
  }

Then open a console and give the command:

.. sourcecode:: bash

  $ echo "eduroam-unimi" >> /etc/wicd/encryption/templates/active

Finally open wicd-client, highlight the connection and press *Properties*.
Select "Use encryption" and from the box select "Eduroam".
Insert username in the form ``name.surname`` and your webmail password. Press
*OK* and then *Connect*.

Enjoy the Internet!
