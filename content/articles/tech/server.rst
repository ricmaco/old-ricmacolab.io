:title: How did I run this site?
:date: 2015-04-22 19:14
:tags: site, hardware, python, static generator, embedded, rpi3, raspberry, pi, nginx
:status: published
:summary: Hardware (and consequent software) which used to serve you this modest site.

.. image:: {filename}/images/articles/rpi3.jpg
   :width: 500px
   :align: right
   :alt: raspberry pi 3 photo

(note that now the site is served through Gitlab Pages on gitlab.com__)

.. __: https://gitlab.com

I have never been a fan of giving away all my informations and personally
produced content to a service provider I don't completely know and trust. I had
a bad experience, when my virtual server provider deleted all my files without
my consent or any prior warning and, remembering that, I actually prefer to
handle my own data by myself.

In order to do that, I need some sort of association from an hostname to my ISP
provided dynamic IP. So I bought the domain you are seing on top, mainly because
its provider offered a dynamic DNS service included. Before that, I used a third
level free dynamic DNS service provided by the LUG__ (Linux User Group) I
attend.

.. __: http://linuxvar.it/content/dynamic-dns

Alas the Internet connection in my country isn't that great, but I, personally
speaking, cannot complain at all. I have an asymetric DSL 12/0.8 Mbps, quite
enough for offering simple services such as HTTP and git. I connect to the
Internet by means of a cheap router that supports high speed ethernet and a
WLAN.

But the real star of this page is the server phisically providing this service,
a `Raspberry Pi 3`__, produced by `Raspberry Foundation`__.  It is an embedded,
single board, but modestly high powered computer, backed by an `ARMv8`_
processor and 1 GB of RAM, running my beloved `Arch Linux`_, ARM version.

.. __: https://www.raspberrypi.org/products/raspberry-pi-3-model-b/
.. __: https://www.raspberrypi.org/
.. _ARMv8: https://en.wikipedia.org/wiki/ARM_architecture#ARMv8-A
.. _Arch Linux: https://www.archlinux.org/

Software
--------

.. image:: {filename}/images/articles/nginx.png
   :width: 170px
   :align: left
   :alt: lighttpd logo

I chose nginx_ as web server, because it is very light in terms of used
resources and easy to configure, although I currently don't use any server side
scripting engine, like PHP or Python. I plan to set up some Python FCGI in order
to provide some small services.

.. _nginx: https://www.nginx.com/

The site is, therefore, static, but not completely handwritten. As stated in the
footer at the bottom of every page, this site is generated with the excellent
static content generator Pelican_.

.. _Pelican: http://getpelican.com/
