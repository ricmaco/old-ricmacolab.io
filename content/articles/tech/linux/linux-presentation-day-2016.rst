:title: Linux Presentation Day 2016
:date: 2016-05-01 13:17
:tags: 2016, linuxvar, linux, talks, presentation, firewall, traffic sniffing,
       appinventor
:status: published
:summary: 2016 (and first) edition of the Linux Presentation Day for our LUG,
          hosted at Gazzada, Varese.

.. image:: {filename}/images/articles/lpd-2016.png
  :width: 200px
  :align: right
  :alt: linux presentation day logo 2016

A new (but not so revolutionary) event is born in Europe to advertise the use of
Linux and the concept of Open Source and Free Software: the Linux Presentation
Day. All European nations estabilished a day, the 30th of April, to organize
some propaganda for the famouse operative system, I like the most.

My LUG, LinuxVar_, has decided to take an active part in this day, but in its
own classic style.We decided to spend this day in a school, teaching to the
students the power of open systems.
We chose the `I.S.I.S Keynes`__ in `Gazzada Schianno`__, Varese.

.. _LinuxVar: http://linuxvar.it/
.. __: http://www.isiskeynes.it/
.. __: http://www.openstreetmap.org/#map=19/45.77349/8.82783&layers=N

A group decided to present an acitivity based on AppInventor_, an MIT
software which allows to develop Android applications in a Scratch_ fashion,
dealing with visual blocks for coding.

.. _AppInventor: http://appinventor.mit.edu/explore/
.. _Scratch: https://scratch.mit.edu/

Me and my friend decided to give an introductory talk about Internet traffic
sniffing, so we presented principal Linux networking tools together with
``nmap`` and Wireshark. My friend centered his attention on a presentation
over firewalls tassonomy and firewalls tasks.

A copy of the presentation on traffic sniffing is available to download here__.

.. __: /files/linuxday/2016p/analisi-rete.pdf
