:title: Linux Day 2018
:date: 2018-11-12 22:16
:tags: 2018, linuxday, linuxvar, linux, talks, www, world wide web, https
:status: published
:summary: 2018 edition of the Linux Day for the Varese area, in Italy.

.. image:: {filename}/images/articles/linuxday2018.png
  :width: 200px
  :align: right
  :alt: linuxday logo 2018

Even this year, together with some organizative difficulties, the `Linux Day`_ time
has come. As every past few years, I was part of the staff team and I held some
talk for the people coming by.

This year the general theme was *World Wild Web*, so I thought of doing some
presentation of what HTTPS actually is and why we should always try to use it
and search for it. That small lock that appears on every browser is really tiny,
but it does a lot of protection work on our behalf.

The usual local associations took their place in the event organization and
hosting. LinuxVar_, LIFO_ laboratory and GL-Como_ LUG joined their forces to
bring some content to the partecipants. For the fourth year, the fair location
was the FaberLab_ room in Tradate.

.. _Linux Day: http://www.linuxday.it
.. _LinuxVar: http://www.linuxvar.it/content/linux-day-sabato-27-ottobre-2018
.. _GL-Como: http://www.gl-como.it/
.. _LIFO: http://lifolab.org
.. _FaberLab: http://www.faberlab.org

Unfortunately, this year no showroom of pratical exibitions or workshops was
there, but there were six talks in the whole afternoon. So, not bad actually.

My own talk
-----------

The presentation slides are of course in Italian and you can view them below.

.. raw:: html

  <object width="100%" height=820 type="application/pdf" data="/files/linuxday/2018/s-in-https.pdf?#zoom=100&scrollbar=0&toolbar=0&navpanes=0">
    <p>error: pdf file cannot be displayed</p>
  </object>

You can also download the presentation slide, in `ODP format`__ or in `PDF
format`__.

.. __: /files/linuxday/2018/s-in-https.odp
.. __: /files/linuxday/2018/s-in-https.pdf

Resources
---------

If you want to see some photos of the event, you can find them on `GL-Como Friendica page`__.

.. __: https://social.gl-como.it/photos/gl-como
