:title: Linux Day 2014
:date: 2014-10-30 14:53
:tags: 2014, linuxday, linuxvar, linux, talks, ltsp
:status: published
:summary: 2014 edition of the Linux Day for the province of Varese, Italy

.. image:: {filename}/images/articles/linuxday2014.png
  :width: 200px
  :align: right
  :alt: linuxday logo 2014

This year too (2014) a `Linux Day`_ edition for the Varese area has been
organized by the LinuxVar_ and GL-Como_ LUG and the LIFO_ laboratory at the
FaberLab_ of Tradate, an interesting place where you can build up your ideas,
make 3D and ceramics printings and find someone who will teach you all these
goodies.

.. _Linux Day: http://www.linuxday.it
.. _LinuxVar: http://www.linuxvar.it/content/linux-day-2014
.. _GL-Como: http://www.gl-como.it/
.. _LIFO: http://lifolab.org
.. _FaberLab: http://www.faberlab.org

As usual, some talks, workshop and projects took place. But this year there has
been quite a substancial difference with respect to past editions, because for
the first time the fair will be targeted exclusively to students.

Me
--

Personally I showed LTSP_, a server-thin client system for schools and
organizations computer laboratories. We provided a small lab-like structure
with three posts and an LTSP server.

.. _LTSP: http://www.ltsp.org

A more descrptive and detailed explanation of this system is present at this
page__, complete with slides used to show it.

.. __: {filename}/articles/talks/ltsp.rst

Furter information and materials about presented talks can be found here__,
under the form of blog post at the LinuxVar site.

.. __: http://www.linuxvar.it/content/slide-linux-day-2014

Some photos of the event can be, instead, downloaded here__, on my file server.

.. __: /files/linuxday/2014/photos.7z
