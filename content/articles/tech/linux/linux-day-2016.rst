:title: Linux Day 2016
:date: 2016-10-29 15:10
:tags: 2016, linuxday, linuxvar, linux, talks, gimp, 0x10, free as in beer,
       arduino
:status: published
:summary: 2016 edition of the Linux Day for the province of Varese, Italy.

.. image:: {filename}/images/articles/linuxday2016.png
  :width: 200px
  :align: right
  :alt: linuxday logo 2016

.. image:: {filename}/images/articles/linuxvar-0x10.jpg
  :width: 200px
  :align: left
  :alt: linuxvar 0x10 cake

In this period of the year, the time has come for the `Linux Day`_ for the area
of Varese, Lombardy..
This edition we, LinuxVar_, LIFO_ laboratory and GL-Como_ LUGs, have decided to
make it take place another time at the FaberLab_ of Tradate, an interesting
place where anyone can transform its ideas into a 3D printed reality.

.. _Linux Day: http://www.linuxday.it
.. _LinuxVar: http://www.linuxvar.it/content/linux-day-2014
.. _GL-Como: http://www.gl-como.it/
.. _LIFO: http://lifolab.org
.. _FaberLab: http://www.faberlab.org

As usual, some talks, workshop and projects took place. This year we decided
that not only the school were to partecipate at this event, but also they were
to be part of the protagonists. So some talks on Arduino by students for
students were organized.

Me
--

I, personally, choose to build up a workshop on the GIMP, the GNU Image
Manuplator Program. Further information on the can be found here__.

.. __: https://www.gimp.org/

A more descrptive and detailed explanation of the workshop is available at this
page__, complete with slides used to show it.

.. __: {filename}/articles/talks/intro-gimp.rst

Some photos of the event can be, instead, downloaded here__, on my file server.

.. __: /files/linuxday/2016/photos.7z
