:title: Linux Day 2015
:date: 2015-10-23 10:24
:tags: 2015, linuxday, linuxvar, linux, talks
:status: published
:summary: 2015 edition of the Linux Day for the province of Varese, Italy

.. image:: {filename}/images/articles/linuxday2015.png
  :width: 200px
  :align: right
  :alt: linuxday logo 2016

This year the `LUG I usually attend`_ and another neighbour LUG, GL-Como_,
organized the 2015 edition of the `Linux Day`_ at the `ISISS Sant'Elia`_ in
Cantù, near Como.

.. _LUG I usually attend: http://www.linuxvar.it/content/linux-day-2015
.. _GL-Como: http://www.gl-como.it/v2015/linux-day-2015/
.. _Linux Day: http://www.linuxday.it
.. _ISISS Sant'Elia: http://www.istitutosantelia.gov.it

We organized some talks and workshops, together with a questionnaire to make
students see and try what the Linux and the open source communty is.

I helped with the Arduino_ workshop and completely designed the questionnaire
about "*young people and the Internet*", which the student have answered on an
LTSP_ (refer here__ to some LTSP slides) post in a Linux adapted laboratory.

.. _Arduino: http://www.arduino.cc
.. _LTSP: http://www.ltsp.org
.. __: /files/linuxday/2014/ltsp.pdf

Information about the Arduino workshop and the other initiatives can be found
here__, with some git repos about materials provided here__.

.. __: http://www.linuxvar.it/context/slide-linux-day-2015
.. __: http://linuxvar.it/gitweb

Furter and detailed information about presented questionnaire can be viewed
here__.

.. __: {filename}/articles/talks/young-people-and-snss.rst

Some photos are to be admired here__ or here__.

.. __: /files/linuxday/2015/photos.7z
.. __: http://www.linuxvar.it/context/foto-linux-day-2015
