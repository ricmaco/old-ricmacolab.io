:title: Linux Day 2017
:date: 2017-10-28 12:37
:tags: 2017, linuxday, linuxvar, linux, talks, instagram, privacy
:status: published
:summary: 2017 edition of the Linux Day for the province of Varese, Italy.

.. image:: {filename}/images/articles/linuxday2017.png
  :width: 200px
  :align: right
  :alt: linuxday logo 2016


The fourth saturady of october has come, which means it is time for `Linux
Day`_. As always I and the group I am a member of organized the edition for the
area of Varese, Lombardy.

We gave force to the usual collaboration with other local associations, such as
us, that is LinuxVar_, LIFO_ laboratory and GL-Como_ LUG. For the third year the
decision on the location fell on the FaberLab_ of Tradate, a place very apt to
organize this types of events, and home of DIY hobbyists who like to tinker
with 3D printing.

.. _Linux Day: http://www.linuxday.it
.. _LinuxVar: http://www.linuxvar.it/content/linux-day-2014
.. _GL-Como: http://www.gl-como.it/
.. _LIFO: http://lifolab.org
.. _FaberLab: http://www.faberlab.org

As every year some talks and some exhibitions of various proofs of concept took
place. The morning being reserved to presentations regarding topics close to the
new generations, such as Internet security, decentralized social network sites
and the powers of computer science and open source.

Me
--

This year I gave a simple talk about Instagram (and all other social network
sites) dangers. You can find more info about the presentation here__.

.. __: {filename}/articles/talks/instagram-instaprivacy.rst

Resources
---------

If you want to see some photos (mainly of the necks of the public) of the
event, you can find them on `GL-Como Friendica page`__.

The resources used for quite all talks are available on the `LinuxVar site`__.

.. __: https://social.gl-como.it/photos/gl-como
.. __: http://linuxvar.it/content/linux-day-2017
