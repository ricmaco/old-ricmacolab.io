:title: Stack Overflow network analysis
:date: 2017-06-12 14:15
:tags: stack overflow, network analysis, graph, big data
:status: published
:summary: A simple and straightforward analysis of the Stack Overflow answer
          graph.

.. image:: {filename}/images/articles/stack-overflow.png
  :height: 150px
  :align: center
  :alt: stack overflow logo

As we all know, a programmer always needs help from another fellow programmer or
sometimes just wants to reach easily the best implementation of an algorithm.

For this purpose, `Stack Overflow`_ has been created.

.. _Stack Overflow: https://stackoverflow.com

Stack Overflow is part of the `Stack Exchange`_ network and at the time of
writing it counts 7.3 milion users, with more than 14 milions question asked and
22m of answers given (`more here`__).

.. _Stack Exchange: https://stackexchange.com/
.. __: https://stackexchange.com/sites?view=list#traffic

We present a simple ans standard analysis of the Stack Overflow constructed as
follows:

* every **node** is a **user**;
* every **edge** answers to the question *has answered to*;
* an optional **weight** has been given if a user has answered more than once
  to another user (but it was not used during the analysis).

A copy of the notebook with the complete analysis is here__.

A presentation of the result obtained can be viewed below (a `pdf version`__ is
available).

.. raw:: html

  <object width="100%" height=1000 type="application/pdf" data="/files/uni/stack-overflow/presentation.pdf?#zoom=100&scrollbar=0&toolbar=0&navpanes=0">
    <p>error: pdf file cannot be displayed</p>
  </object>

.. __: /raw/stack-overflow/notebook.html
.. __: /files/uni/stack-overflow/presentation.pdf
