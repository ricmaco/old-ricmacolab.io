:title: Introduction to Machine Learning
:date: 2018-03-23 00:45
:tags: 2018, talk, introduction, machine learning
:status: published
:summary: Introductory talk to machine learning concepts.

.. image:: {filename}/images/articles/perceptron.png
  :width: 50%
  :align: center
  :alt: perceptron schematics

As I stated in some article before, in the LUG I use to attend, we give some
talks to try to share as much knowledge as possible, over our current interests.

I always try to join these events, because they offer me a unique occasion to
train my presentation skills and to write down some of the concepts I am
currently looking into.

In this time period, I just finished a university lecture over some machine
learning concepts. Not that I consider myself an expert in this field, but I
feel confident enough to explain the basics to completely agnostic people.

So I dediced to give an introductory talk about this topic. The talk night took
place the 21\ :sup:`st` of March, 2018. The talk has been recorded on video
(more info later).

Presentation
------------

The presentation slides are of course in Italian and you can view them below.

.. raw:: html

  <object width="100%" height=820 type="application/pdf" data="/files/linuxvar/intro_machine_learning/intro_machine_learning.pdf?#zoom=100&scrollbar=0&toolbar=0&navpanes=0">
    <p>error: pdf file cannot be displayed</p>
  </object>

You can also download the presentation slide, in `ODP format`__ or in `PDF
format`__.

.. __: /files/linuxvar/intro_machine_learning/intro_machine_learning.odp
.. __: /files/linuxvar/intro_machine_learning/intro_machine_learning.pdf

Video
-----

The video recording of the night is available on YouTube, through the `LinuxVar
channel`__. (I know the caption title of the video is wrong, but the
presentation is the right one)

.. __: https://www.youtube.com/channel/UCoYyCUclcvKVnvyfgla4I0g

.. raw:: html

  <div class="video-container">
    <iframe class="video" allowfullscreen="" frameborder="0"
      src="https://www.youtube.com/embed/ihQgyewHw9c">
    </iframe>
  </div>

Of course, if you have any questions, you can write me using one of the contact
badges on the page aside.
