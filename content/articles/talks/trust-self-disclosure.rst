:title: Trust & Self-disclosure in online interactions (focusing on young people)
:date: 2015-05-02 22:54
:tags: trust, self-disclosure, social network, young people
:status: published
:summary: In a virtuality ruled by computer-mediated communications, it is
          important to know how young people evaluate risk introduced of
          self-disclosure

By Riccardo Macoratti and Stanislao Vezzosi

Bibliography
============

Bryce, J. and Fraser, J. (2013) "*The role of disclosure of personal information
in the evaluation of risk and trust in young peoples' online interactions*",
Computers in Human Behavior, 30(2014): 299-306.

Bryce, J. and Klang, M. (2009) "*Young people, disclosure of personal
information and online privacy: Control, choice and consequences*", Information
security technical report, 14(2009): 160–166.

Taddei, S. and Contena, B. (2012) "*Privacy, trust and control: Which
relationships with online self-disclosure?*", Computers in Human Behavior,
29(2013): 821–826.

Sitography
==========

Wikipedia, `Trust (social sciences)`_

.. _Trust (social sciences): https://en.wikipedia.org/wiki/Trust_%28social_sciences%29

Wiktionary, `Trust`_

.. _Trust: https://en.wiktionary.org/wiki/trust

Slides
======

Slides are made with Prezi_.

.. _Prezi: https://prezi.com/

For people who like to travel the Internet with Adobe Flash support disabled (I don't blame you,
but more information on Adobe Flash can be found here_), a
PDF file is provided.

.. _here: https://get.adobe.com/it/flashplayer/

Introduction
------------

.. raw:: html

  <iframe src="https://prezi.com/embed/xno44xarjdce/?bgcolor=ffffff&amp;lock_to_path=1&amp;autoplay=0&amp;autohide_ctrls=0#" allowfullscreen=""
  mozallowfullscreen="" webkitallowfullscreen="" frameborder="0" height="400" width="550"></iframe>

Download__

.. __: /files/uni/trust-self-disclosure/trust-self_disclosure-introduzione.pdf

Young people
------------

.. raw:: html

  <iframe src="https://prezi.com/embed/udsvkw2f68qb/?bgcolor=ffffff&amp;lock_to_path=1&amp;autoplay=0&amp;autohide_ctrls=0#" allowfullscreen=""
  mozallowfullscreen="" webkitallowfullscreen="" frameborder="0" height="400" width="550"></iframe>

Download__

.. __: /files/uni/trust-self-disclosure/trust-self_disclosure-giovani.pdf

Questionary
===========

A summary for our questionary "**Do computer science students trust SNSs?**" is
available in this PDF_.

.. _PDF: /files/uni/trust-self-disclosure/trust_in_sns_questionary.pdf
