:title: Git in pratica
:date: 2017-02-16 15:05
:tags: talk, git, programming, streaming
:status: published
:summary: Video tutorial, with presentation on everyday git use and commands.

.. image:: {filename}/images/articles/git-in-pratica.png
  :height: 70px
  :align: center
  :alt: git in pratica talk logo

I already presented on this blog git__, the SCM (**S**\oftware,
**C**\onfiguration, **M**\anagemer) of choiche. Last time, I made a simple
introdutory tutorial on the system, showing its potential, but I didn't dwell
enough on the everyday use. I decided to organize another talk in which all day
use and commands were the center of the conversation.

.. __: https://git-scm.com/

Video recap
-----------

This time, I chose to record myself talking, instead of only providing
presentation material. So, here it is, the video recap of the evening.

.. raw:: html

  <div class="video-container">
    <iframe class="video" allowfullscreen="" frameborder="0"
      src="https://www.youtube.com/embed/h2t1opDnd38">
    </iframe>
  </div>

Presentation
------------

The presentation being shown in the video is provided below.

.. raw:: html

  <div class="video-container">
    <iframe class="video"
      src="/raw/git-in-pratica/presentation.html#1">
    </iframe>
  </div>

Download the presentation__ or `watch it fullscreen`__.

.. __: /files/linuxvar/git_in_pratica/git_in_pratica.zip
.. __: /raw/git-in-pratica/presentation.html

Useful information
------------------

The shell used during the video stream is the `fish shell`__.

.. __: https://fishshell.com/

If you desire an informative prompt about the situation of the git repository in
the current directory, you have a couple of viable options.

- Bash

  - git-prompt.sh_
  - bash-git-prompt__

- Zsh

  - zsh-git-prompt__
  - oh-my-zsh__
  - git-prompt.sh__

- Fish
  
  - `__fish_git_prompt.fish`__
  - oh-my-fish__

.. _git-prompt.sh: https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh
.. __: https://github.com/magicmonty/bash-git-prompt
.. __: https://github.com/olivierverdier/zsh-git-prompt
.. __: https://github.com/robbyrussell/oh-my-zsh
.. __: git-prompt.sh_
.. __: https://github.com/fish-shell/fish-shell/blob/master/share/functions/__fish_git_prompt.fish
.. __: https://github.com/oh-my-fish/oh-my-fish

If you want to have the alias ``hist``, in order to use the command ``git
hist`` as in the video, you should download the file gitconfig__ and copy it at
``$HOME/.gitconfig``.

.. __: /files/linuxvar/git_in_pratica/gitconfig
