:title: Introduction to Python 3
:date: 2016-06-01 22:30
:tags: talk, python, python3, programming
:status: published
:summary: A basic introduction to the Python language version 3.

.. image:: {filename}/images/articles/python.png
  :height: 200px
  :align: right
  :alt: python3 logo

A language that is increasingly becoming more and more famous and prominent,
both as a scripting language and a scientifical tool, is Python_.

.. _Python: https://www.python.org/

Python lets newcomers accomodate really quickly and easily. Discover it
yourself!

Here__ I provide a small introduction to basic concepts of becoming a
*pythonista*.

.. __: /raw/intro-python/presentation.html

If you are interested you can download a zip__ containing the presentation.

.. __: /files/linuxvar/intro_python/introduzione_python.zip
