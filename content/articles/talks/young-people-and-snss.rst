:title: Young People and SNSs
:date: 2015-10-26 20:01
:tags: young people, social network
:status: published
:summary: How do young people use social network sites?

In an Internet reality populated by real virtualities_, in which young people
is completely absorbed in everyday life too, it is becoming more and more
fundamental to discuss and understand how young people (13-20) use these new
technologies and how much understand of their possible dangers.

.. _virtualities: https://en.wikipedia.org/wiki/Virtuality_(philosophy)

I ideated a small questionnaire to gather some data about what personal
informations are usually provided to social network sites by young people and
how they interact with this huge amount of data.

The questionnaire was aired on October 24, 2015 at the `Linux Day 2015 of
Varese`__ in the `ISISS Antonio Sant'Elia`__, an Italian high school, featuring
the students as questionee.

.. __: http://linuxvar.it/content/linux-day-2015
.. __: http://www.istitutosantelia.gov.it

I took the opportunity and displayed the results in a small presentation, where
in the last slides I talked about Facebook T.O.S. (**T**\erms **o**\f
**S**\ervice) as an example of social network site licence agreement.

Slides can be found here__.

.. __: /files/linuxday/2015/questionario.zip

This, instead, is a chart of the results:

.. image:: {filename}/images/articles/questionnaire.jpg
  :width: 100%
  :align: center
  :alt: questionnary aggregations displayed as image
