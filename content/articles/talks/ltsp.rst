:title: LTSP
:date: 2014-10-30 12:20
:tags: ltsp, terminal server
:status: published
:subtitle: Some slides about the Linux Terminal Server Project.

.. image:: {filename}/images/articles/ltsp.png
  :height: 150px
  :align: right
  :alt: ltsp logo

LTSP_ is a server-client technology built in order to fulfill the computing
needs of small officies and educational environments.

.. _LTSP: http://www.ltsp.org/

The typical system is a dedicated network in which the server is a powerful
elaborator and the clients (known as **thin clients**) are
computers without rotative parts, such as a hard disk, with only a motherboard,
a small amount RAM, a very modest CPU, GPU and I/O peripherals (monitor,
keyboard and mouse).

The server provides computation power, network connection and a complete
operative system, while clients only draw the graphics and handle I/O.

I made a quick and simple presentation of LTSP features, with a stress on how
to build a LTSP system easily.

Slides can be downloaded here__ in PDF format and here__ in ODP format.

.. __: /files/linuxday/2014/ltsp.pdf
.. __: /files/linuxday/2014/ltsp.odp
