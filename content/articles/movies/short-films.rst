:title: Some short films
:date: 2015-04-18 18:53
:tags: short movies, stop motion, chroma key
:status: published
:subtitle: Just a dump of some clips I produced for a multimedia project exam at
           university.

Ok, my directing, editing and producing ability is not at its finest, although
I'm learning. It is not my field, though, so I do not expect to improve much.

I had to realize two clips: one using the stop motion technique and one using a
chroma key footage. Here they are:

.. raw:: html

  <div class="video-container" style="margin-bottom: 10px;">
    <iframe class="video" src="https://www.youtube.com/embed/AKndch7vk-E"
      frameborder="0" allowfullscreen></iframe>
  </div>

.. raw:: html

  <div class="video-container">
    <iframe class="video" src="https://www.youtube.com/embed/nhOYaf9BSEU"
      frameborder="0" allowfullscreen></iframe>
  </div>

Quite amatorial, right? Well, it's a start.