:title: La Nuit Derniére a Parìs
:date: 2015-09-20 14:17
:tags: nighthawks, mafia, thriller, crime novel
:status: published
:summary: A short film produced for a multimedia project exam at university.

As my last exam for the Digital Communication degree I chose to produce
a short film inspired by the work "*Nighthakws*", by `Edward Hopper`_.

.. _Edward Hopper: https://en.wikipedia.org/wiki/Edward_Hopper

If you want to know more about Hopper's work check out `his page`__ on `Artsy`__, a
promising online art gallery.

.. __: https://www.artsy.net/artist/edward-hopper
.. __: https://www.artsy.net/about

.. image:: {filename}/images/articles/nighthawks.jpg
  :width: 100%
  :align: center
  :alt: nighthawks picture

The story is about two mafia bosses (one being a female) and a cop protagonist
who struggles to arrest them. In this journey, he will score some triumphs and
suffer some defeats.

If you're interested, here__ a copy of the script may be downloaded.

.. __: /files/uni/la-nuit-derniere-a-paris/la-nuit-derniere-a-paris_script.pdf

What remains to be said? Let's watch it.

.. raw:: html

  <div class="video-container">
    <iframe class="video" src="https://www.youtube.com/embed/3djXnzmdaAU"
      frameborder="0" allowfullscreen></iframe>
  </div>
