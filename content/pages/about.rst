:title: Bio
:category: menu
:tags: about, bio
:status: published
:summary: Bio of the author of this site.

.. image:: {filename}/images/pages/qrcode.jpg
   :width: 150px
   :alt: qrcode representing site web address
   :align: right

Hello, world!

I am Riccardo Macoratti, a young man fresh graduated in computer science. I 
also happen to be the owner and creator of this site.

Some quick information about myself: I was born at the end of 1992 in Rho_, a
relatively big center near Milan. I currently live `Somma Lombardo`_, a small
city located in Lombardy.

.. _Rho: https://www.openstreetmap.org/relation/45188
.. _Somma Lombardo: https://www.openstreetmap.org/relation/45572

Studies
=======

When I was 14 I had to make a choice about which school to attend in the future.
I actually chose a `high school`_ specializing in modern languages, which means 
that I learnt English, Spanish and German. As a result, I am a quite fluent 
English speaker, but I also remeber (and like) Spanish quite well. 
Unfortunately, these years I haven't had much chanches of speaking German, but 
I'd really love to.

.. _high school: http://www.liceimanzoni.it/

*(I consider myself proficient in English, but after all it is not my
mothertongue, so if you find some grammatical error, I apologize in advance.
Feel, also, free to contact me to notice.)*

At 19 years old, I understood languages were not my way, although I liked them.
Nonetheless, I was becoming more and more interested in technology, directing 
my interest in the open source community.
Over the years, messing with my faithful (and only) computer, I slowly started
to learn how to automate some repetitive processes of my digital life or, as I
later discovered, I was simply learning how to program: I knew I wanted to
be a computer scientist.

So, I began attending a Bacherlor's degree course in `Digital Communication`_ 
at the `University of Milan`_. I graduated in December 2015 with a mark of
110/110 submitting a dissertation entitled "*Design and creation of a digital 
image capturing system and streaming towards a distributed player*".
A digital copy of the disseration can be obtained here__.

In January 2015, I began a Master's degree course in `Computer Science`_ also at
the `University of Milan`_. In April 2018, I graduated with a mark of 109/110, 
producing a dissertation called "*An application for remote competitions 
system: analysis, revision and implementation of new features*".
A digital copy of the disseration can be obtained here__.

.. _Digital Communication: http://www.ccdinfmi.unimi.it/it/corsiDiStudio/2015/F2Xof1/manifesto.html
.. _Computer Science: http://www.ccdinfmi.unimi.it/it/corsiDiStudio/2017/F94of2/manifesto.html
.. _University of Milan: http://www.unimi.it/ENG/
.. __: /files/uni/tesi/tesi.pdf
.. __: /files/uni/tesi/tesi-magistrale.pdf

The second dissertation lead to a pubblication at the `CSEDU 2018`_ conference. 
The poster of the pubblication won the *Best Poster Award* prize.
A digital copy of the disseration can be obtained here__ and of the poster, 
here__.

.. _CSEDU 2018: http://www.csedu.org/?y=2018
.. __: /files/uni/tesi/csedu2018.pdf
.. __: /files/uni/tesi/csedu2018-poster.pdf

Interests
=========

.. image:: {filename}/images/pages/neo-tux.png
  :width: 150px
  :alt: tux, the linux logo, image, dressed as neo in matrix
  :align: right

As I stated before, I really and deeply **love** the open source world, and my
appreciation is directed especially towards GNU/Linux, by far my operating
system of choice.

In 2014 I started attending a small LUG (Linux User Group) called LinuxVar_.
We are actually more focused on spreading the GNU/Linux word, but sometimes we
work on some little projects, helping each other and sharing knowledge.

Once in a while we give talks and lessons to high school students, not only on
GNU/Linux, but more in general on open source technologies and computer science.

Lastly, every so often, we organize themed nights to speak about an interesting
open source matter or application, where whoever feels more condifent may
arrange a small presentation on the subject, thus sharing the knowledge.

.. _LinuxVar: http://www.linuxvar.it

Some experiences I had, made me develop an interest in video-related and, more
in general, vision-related technologies. In 2015 I had the opportunity to
attend a traineeship in a company and to experiment with video capturing
devices, video projectors, streaming protocols and videowalls.
I architectured and built an application, also subject of my graduation thesis,
called "V4LCapture" which captures a video stream from a video capture board
and sends it to a distributed player, commonly known as a videowall.
You can peek it__ on Gitlab.

.. __: https://gitlab.com/datasoftsrl/v4lcapture

Recently I also developed a dual pane web file manager to help cinemas
transferring movies all around network attached storages to projectors. The
source is also available here__ on Gitlab.

.. __: https://gitlab.com/datasoftsrl/cinefm

I really hope you enjoy reading some content I write here and if you want to
contact me, feel free to, referring to one of the links in the sidebar.
