:title: Cookie policy
:category: info
:tags: cookies, policy, data, third-party
:status: hidden
:summary: Learn what will be done with your data.

The physical machine that is hosting this site is property of site owner, thus
based in Italy.

With reference to the legislative measure "`Individuazione delle modalità
semplificate per l'informativa e l'acquisizione del consenso per l'uso dei
cookie`__" of 8 May 2014 (published onto the Official Journal no. 126 of 3 June
2014), characteristics and aims of cookies used throughout this site are
described specifically and analitically.

.. __: http://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/3118884

This website, "**RicMa.co**", from now on only "**site**", does not use any
first-part profiling cookies; the only cookie used is considered *technical* and
serves the scope to make the site remember you accepted this cookie policy
(*ironically*).

On the other side, third-part cookies, either technical or profiling, may be
served on this site (but an attempt is made to keep them at a minimum). If you
want to learn more about their cookie policy, refer to their site.

More information on how to protect your online privacy (for European countries)
can be found at `Your Online Choices`_.

.. _Your Online Choices: http://www.youronlinechoices.com/

Some links to relevant third-party policies are put here for convenience:

- `Google Inc. (Youtube included)`_
- `Prezi Inc.`_

.. _Google Inc. (Youtube included): http://www.google.it/intl/it/policies/privacy/
.. _Prezi Inc.: https://prezi.com/terms-of-use/
