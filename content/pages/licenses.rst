:title: Licenses
:category: info
:tags: license, creative commons, fonts
:status: hidden
:summary: All licensing matters of this site

This site and all its content are licensed under the `Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International`__, unless othewise
specified (such as for the theme, which is used and provided under `MIT
License`__.

.. __: http://creativecommons.org/licenses/by-nc-sa/4.0/
.. __: https://opensource.org/licenses/MIT

The rights for the site name and logo, favicon and images of the author
(wherever found) are all reserved.

The fonts used are:

- `Special Elite`_, by Astigmatic_, licensed under the `Apache License, 2`_.
- `Ruda`_, by `Marie Monsalve`_ and `Angelina Sanchez`_, licensed under the
  `SIL Open Font License, 1.1`_.

.. _Special Elite: http://www.fontsquirrel.com/fonts/special-elite/
.. _Astigmatic: http://www.astigmatic.com/
.. _Apache License, 2: https://www.apache.org/licenses/
.. _Ruda: http://www.google.com/fonts/specimen/Ruda
.. _Marie Monsalve: http://www.mukamonsalve.com.ar/
.. _Angelina Sanchez: http://www.angelinasanchez.com.ar/
.. _SIL Open Font License, 1.1: http://scripts.sil.org/OFL

Images contained in this site, unless otherwise specified, belong to their
respective copyright owners and, possibly, are therefore used under "fair use".
